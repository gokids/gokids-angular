// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // AUTH_API : 'http://localhost:9999',
  // baseUrl : 'http://localhost:3000',

  AUTH_API : 'https://gokidsuser.herokuapp.com',
  baseUrl : 'https://gokids-crud-staging.herokuapp.com'

 

  

};

