import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { CartComponent } from './components/cart/cart.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { EvenementslistComponent } from './components/evenementslist/evenementslist.component';

import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MapEventComponent } from './components/map-event/map-event.component';
import { ProductsComponent } from './components/products/products.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { StatsComponent } from './components/stats/stats.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path:'home', component: HomeComponent},
  {path:'about', component: AboutComponent},
  {path:'register', component: RegisterComponent},
  // {path:'list', component: EvenementslistComponent},
  {path:'contactus', component: ContactusComponent},
  {path:'footer', component: FooterComponent},
  {path:'login', component: LoginComponent},
  {path: 'user', component: BoardUserComponent},
  {path: 'admin', component: BoardAdminComponent},
  {path: 'profile', component: ProfileComponent },
  {path: 'stats', component: StatsComponent},
  {path: 'maps', component: MapEventComponent},
  {path: 'list', component: ProductsComponent},
  {path: 'cart', component: CartComponent},

 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

