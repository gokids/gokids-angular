import { Component, OnInit } from '@angular/core';
import { EvenementsService } from 'src/app/_services/evenements.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent {

  EVENEMENTS: any;
  
  constructor(private evenementsService: EvenementsService) {
  }


  ngOnInit(): void {
    this.retrieveEvents();
  }  

  retrieveEvents(): void {
    this.evenementsService.getAll()
      .subscribe(
        response => {
          this.EVENEMENTS = response;
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  listEvenementGratuit(){
    let evenementsGratuit = [];
    for (let e of this.EVENEMENTS){
      if (e.price_type == "gratuit")
      evenementsGratuit.push(e)
    }
    return evenementsGratuit;
  }

  nombreEvenementGratuit(){
    let nombreEvenementsGratuit = 0;
    for (let e of this.EVENEMENTS){
      if (e.price_type == "gratuit")
      nombreEvenementsGratuit ++;
    }
    return nombreEvenementsGratuit;
  }

  nombreEvenementGratuitSousCondition(){
    let nombreEvenementGratuitSousCondition = 0;
    for (let e of this.EVENEMENTS){
      if (e.price_type == "gratuit sous condition")
      nombreEvenementGratuitSousCondition ++;
    }
    return nombreEvenementGratuitSousCondition;
  }

  nombreEvenementPayant(){
    let nombreEvenementsPayant = 0;
    for (let e of this.EVENEMENTS){
      if (e.price_type == "payant")
      nombreEvenementsPayant ++;
    }
    return nombreEvenementsPayant;
  }

  nomEventpremier(){
    let nomEventpremier = 0;
    for (let e of this.EVENEMENTS){
      if (e.address_zipcode== "75001")
      nomEventpremier ++;
    }
    return nomEventpremier;
  }

  nomEventsecond(){
    let nomEventsecond = 0;
    for (let e of this.EVENEMENTS){
      if (e.address_zipcode== "75002")
      nomEventsecond ++;
    }
    return nomEventsecond;
  }

  nomEventthree(){
    let nomEventthree = 0;
    for (let e of this.EVENEMENTS){
      if (e.address_zipcode== "75003")
      nomEventthree ++;
    }
    return nomEventthree;
  }








}