import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-event',
  templateUrl: './map-event.component.html',
  styleUrls: ['./map-event.component.css']
})
export class MapEventComponent  {
  
  lat: number = 51.678418;
  lng: number = 7.809007;
}