import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/_services/api.service';
import { CartService } from 'src/app/_services/cart.service';

interface Myevent {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  disableSelect = new FormControl(false);

  public productList: any;
  public filterCategory: any
  searchKey: string = "";
  selectedValue: string = "";

  selectFormControl = new FormControl('', Validators.required);



  constructor(
    private api: ApiService,
    private cartService: CartService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.api.getProduct()
      .subscribe(res => {
        this.productList = res;
        this.filterCategory = res;
        this.productList.forEach((a: any) => {
          if (a.price_type === "gratuit" || a.price_type === "gratuit sous condition") {
            a.price_type = "gratuits"
          }
          Object.assign(a, { quantity: 1 });
        });
        console.log(this.productList)
      });

    this.cartService.search.subscribe((val: any) => {
      this.searchKey = val;
    })
  }

  filter(price_type: string) {
    this.filterCategory = this.productList
      .filter((a: any) => {
        if (a.price_type == price_type || price_type == '') {
          return a;
        }
      })
  }

  onChange(event: any) {
    this.selectedValue = event;
    this.filterPostal(this.selectedValue);
  }

  filterPostal(address_zipcode: string) {
    this.filterCategory = this.productList
      .filter((a: any) => {
        if (a.address_zipcode == address_zipcode || address_zipcode == '') {
          return a;
        }
      })
  }

  openScrollableContent(longContent: any) {
    this.modalService.open(longContent, { scrollable: true, size: 'xl', centered: true });
  }

  addtocart(item: any) {
    this.cartService.addtoCart(item);
  }





}