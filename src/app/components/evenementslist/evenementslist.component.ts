import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IEvenement } from 'src/app/models/evenement';
import { CartService } from 'src/app/_services/cart.service';
import { EvenementsService } from 'src/app/_services/evenements.service';

import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';


interface Myevent{
    value: string;
    viewValue: string;
  }




@Component({
  selector: 'app-evenementslist', 
  templateUrl: './evenementslist.component.html',
  styleUrls: ['./evenementslist.component.css']
  
})

export class EvenementslistComponent implements OnInit {

  filteredString: string = '';

  EVENEMENTS: any;
  page = 1;
  count = 0;
  tableSize = 6;
  tableSizes = [6, 12];
  currentIndex = -1;

  detailForm: FormGroup | undefined;

  today : string = new Date().toISOString();
  dialog: any;
  closeResult: string | undefined;

  selectedValue: string ="";

  



 myevents: Myevent[] = [
    {value: "listEvenementfirst", viewValue: '75001'},
    {value: "filterPostal('75002')", viewValue: '75002'},
   
  ];
  

 
  constructor(
    private evenementsService: EvenementsService, 
    private modalService: NgbModal,
    private cartService: CartService,
    ) {}
  
  
  ngOnInit(): void {
    this.retrieveEvents();
  }  

  retrieveEvents(): void {
    this.evenementsService.getAll()
      .subscribe(
        response => {
          this.EVENEMENTS = response;
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  listEvenementGratuit(){
    let evenementsGratuit = [];
    for (let e of this.EVENEMENTS){
      if (e.price_type == "gratuit")
      evenementsGratuit.push(e)
    }
    return evenementsGratuit;
  }

  nombreEvenementGratuit(){
    let nombreEvenementsGratuit = 0;
    for (let e of this.EVENEMENTS){
      if (e.price_type == "gratuit")
      nombreEvenementsGratuit ++;
    }
    return nombreEvenementsGratuit;
  }


  onTableDataChange(event: any){
    this.page = event; 
    this.retrieveEvents();
  }

  onTableSizeChange(event: any):void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.retrieveEvents();
  } 

  openScrollableContent(longContent: any) {
    this.modalService.open(longContent, { scrollable: true, size: 'xl', centered: true });
  }

  addtocart(evenement: any){
    this.cartService.addtoCart(evenement);
  }

  listEvenementfirst(){
    let listEvenementfirst = [];
    for (let e of this.EVENEMENTS){
      if (e.address_zipcode == "75001")
      listEvenementfirst.push(e)
    }
    return listEvenementfirst;
  }

  drop(event: CdkDragDrop<IEvenement[]>) {
    moveItemInArray(this.EVENEMENTS, event.previousIndex, event.currentIndex);
  }



}  

 


 