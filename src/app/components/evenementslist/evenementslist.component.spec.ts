import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvenementslistComponent } from './evenementslist.component';

describe('EvenementslistComponent', () => {
  let component: EvenementslistComponent;
  let fixture: ComponentFixture<EvenementslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvenementslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvenementslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
