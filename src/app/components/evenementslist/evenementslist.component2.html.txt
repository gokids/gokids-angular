<section class="section" id="about">
  <div class="container">

    <div class="container">
      <div class="row height d-flex justify-content-center align-items-center">
        <div class="col-8">
          <div class="search"> <i class="fa fa-search"></i>
            <input type="text" class="form-control" placeholder="Que recherchez-vous ? " [(ngModel)]="filteredString" />
            <button class="btn btn-primary">OK</button>
          </div>
        </div>
      </div>
    </div>
    <br>

    <div>
      <p>Total des événements {{EVENEMENTS.length}}</p>
      <p>Total des événements gratuit : {{nombreEvenementGratuit()}}</p>
    </div>

    <div class="row ">
      <!-- CARD-->
      <div class="col-lg-4 p-3" *ngFor="let evenement of EVENEMENTS| paginate : {
      itemsPerPage: tableSize,
      currentPage: page,
      totalItems: count} | filter: filteredString;
      let i = index">

        <div class="card h-100" *ngIf="evenement.cover_url">
          <img [src]="evenement.cover_url" class="card-img-top" alt="..." />
          <div class="card-body">
            <h5 class="card-title">{{ evenement.title }}</h5>
            <h6 class="card-title"> du {{ evenement.date_start |date}} au {{ evenement.date_end |date}} </h6>
            <h6 class="card-title">{{ evenement.address_zipcode}}</h6>
            <h6 class="card-title">{{ evenement.price_type}}</h6>
            <button mat-raised-button color="warn" (click)="openScrollableContent(longContent)">Détails</button>

            <ng-template #longContent let-modal>
              <!-- <div class="modal-content"> -->
                <div class="modal-header">
                  <h3 class="modal-title" style="color:#ffffff">{{ evenement.title }}</h3>
                  <button type="button" class="close" aria-label="Close" style="color:#ffffff"
                    (click)="modal.dismiss('Cross click')">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <div class="modal-body">

                  <!-- <div class="row">


                  <div class="col-4" *ngIf="evenement.cover_url">
                    <img [src]="evenement.cover_url" alt="...">
                  </div>

                  <div class="col-8">
                    <h6>{{ evenement.lead_text}}</h6>
                  </div>


                </div>
                <br> -->

                  <div class="row">
                    <div class="col-4" *ngIf="evenement.cover_url">
                      <img [src]="evenement.cover_url" alt="...">
                      <br>

                      <div class="row">
                        <div class="col-12">

                          <br>
                          <h6 style="color:#fb5849"><strong>DATE ET HEURE </strong> </h6>
                          <h6> du {{ evenement.date_start |date: 'd MMM y, h:mm'}}</h6>
                          <h6> au {{ evenement.date_end |date: 'd MMM y, h:mm'}}</h6>
                          <hr />

                          <h6 style="color:#fb5849"><strong>ADRESSE</strong> </h6>
                          <h6 *ngIf="evenement.address_name"> {{ evenement.address_name}}</h6>
                          <h6 *ngIf="evenement.address_street"> {{ evenement.address_street}}, {{ evenement.address_zipcode}} Paris, France </h6>
                       
                          <hr />

                          <h6 style="color:#fb5849"><strong>TARIF</strong> </h6>
                          <h6> {{ evenement.price_type}}</h6>
                          <h6 *ngIf="evenement.price_detail"> {{ evenement.price_detail}}</h6>
                          <hr />


                        </div>

                      </div>
                    </div>
                    <div class="col-8">
                      <h6 style="color:#fb5849" *ngIf="evenement.lead_text"><strong>{{ evenement.lead_text}}</strong></h6>
                      <br>

                      <h6 [innerHTML]="evenement.description"></h6>
                    </div>

                  </div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-light" (click)="modal.close('Close click')">Close</button>
                </div>

              <!-- </div> -->




            </ng-template>
          </div>


        </div>
      </div>

    </div>
  </div>

  <!-- PAGINATION -->
  <div class="d-flex justify-content-center mb-3">
    <pagination-controls [responsive]="true" previousLabel="Prev" nextLabel="Next"
      (pageChange)="onTableDataChange($event)">
    </pagination-controls>
  </div>

  <div class="d-flex flex-row-reverse bd-highlight mb-2">
    <div class="p-2 bd-highlight">
      <select (change)="onTableSizeChange($event)" class="custom-select">
        <option *ngFor="let size of tableSizes" [ngValue]="size">
          {{ size }}
        </option>
      </select>
    </div>
  </div>



</section>