import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AboutComponent } from './components/about/about.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterComponent } from './components/register/register.component';
import { EvenementslistComponent } from './components/evenementslist/evenementslist.component';

import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatCardModule} from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { ProfileComponent } from './components/profile/profile.component';

import {MatButtonModule} from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import {MatNativeDateModule} from '@angular/material/core';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from './Pipes/filter.pipe';
import { AgmCoreModule } from '@agm/core';
import { MapEventComponent } from './components/map-event/map-event.component';
import { ProductsComponent } from './components/products/products.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CartComponent } from './components/cart/cart.component';
import { MatSelectModule } from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatChipsModule} from '@angular/material/chips';
import { DragDropModule } from '@angular/cdk/drag-drop';



registerLocaleData(localeFr, 'fr');


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AboutComponent,
    ContactusComponent,
    FooterComponent,
    EvenementslistComponent,
    BoardAdminComponent,
    BoardUserComponent,
    ProfileComponent,
    HeaderComponent,
    HomeComponent,
    FilterPipe,
    MapEventComponent,
    ProductsComponent,
    CartComponent,

    
    
   
  
  ],
 

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatNativeDateModule,
    MatPaginatorModule,
    NgxPaginationModule,
    NgbModule,
    NgSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAZSiXnViGVmGX0Qf1rmixni9GkXJ0ZaPM'
    }),

    MatSelectModule,
    MatRadioModule,
    MatChipsModule,
    DragDropModule




    
  ],
  providers: [ { provide: LOCALE_ID, useValue: "fr-FR" }, authInterceptorProviders],
  bootstrap: [AppComponent],

})
export class AppModule { }
