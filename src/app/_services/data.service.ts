import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IEvenement } from '../models/evenement';

// const baseUrl = 'http://localhost:3000/api/events/';
const baseUrl= environment.baseUrl +'/api/events/';

@Injectable({providedIn: 'root'})
export class DataService {
  
  constructor(private http: HttpClient) { }

  getAll(): Observable<IEvenement[]>{
    return this.http.get<IEvenement[]>(baseUrl);
  }

  // getAllPosts(): Observable<any> {
  //   return this.http.get(baseUrl);
  // }



  get(id: any) {
    return this.http.get(`${baseUrl}/${id}`);
  }
  
  create(data: any) {
    return this.http.post(baseUrl, data);
  }

  update(id: any, data: any) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll() {
    return this.http.delete(baseUrl);
  }

  findByTitle(title: any) {
    return this.http.get(`${baseUrl}?title=${title}`);
  }

}