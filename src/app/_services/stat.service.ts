import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IEvenement } from '../models/evenement';
import * as L from 'leaflet';


const baseUrl= environment.baseUrl +'/api/events/';

@Injectable({providedIn: 'root'})
export class EvenementsService {
  
    
    
  constructor(private http: HttpClient) { }

  getAll(): Observable<IEvenement[]>{
    return this.http.get<IEvenement[]>(baseUrl);
  }

  makeCapitalMarkers(map: L.Map): void {
    this.http.get(baseUrl).subscribe((res: any) => {
      for (const c of res.features) {
        const lat = c.geometry.coordinates[0];
        const lon = c.geometry.coordinates[1];
        const marker = L.marker([lon, lat]).addTo(map);
      }
    });
  }
}