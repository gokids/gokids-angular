import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IEvenement } from '../models/evenement';

// const baseUrl = 'http://localhost:3000/api/events/';
const baseUrl= environment.baseUrl +'/api/events/';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }

  getProduct(): Observable<IEvenement[]>{
    return this.http.get<IEvenement[]>(baseUrl)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  // getAll(): Observable<IEvenement[]>{
  //   return this.http.get<IEvenement[]>(baseUrl);
  // }

}