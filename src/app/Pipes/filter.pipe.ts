import { Pipe, PipeTransform } from '@angular/core';
import { IEvenement } from '../models/evenement';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

//   transform(projectList: Array<any>, search: string): any {
//     if (projectList && search)
//       return projectList.filter(
//         (d) =>
//           d.title.indexOf(search) > -1 ||
//           d.id == search ||
//           d.address_zipcode.indexOf(search) > -1
//       );
//     return projectList;
//   }
// }

// transform(evenement: Array<any>, searchPattern: string) : any {
//   let result="";
//   if(searchPattern){      
//     return evenement.filter( item=> {
//       const title = item.title.toLowerCase().includes(searchPattern.toLowerCase());  
//       const address_zipcode = item.address_zipcode.toLowerCase().includes(searchPattern.toLowerCase()); 
//       const price_type = item.price_type.toLowerCase().includes(searchPattern.toLowerCase());  
//       return (title || address_zipcode || price_type);
//     })
//   }
//   return evenement;
// }


transform(value : any[], filterString: string, propName:string): any[] {
  const result:any =[];
  if(!value || filterString==='' || propName ===''){
    return value;
  }
  value.forEach((a:any)=>{
    if(a[propName].trim().toLowerCase().includes(filterString.toLowerCase())){
      result.push(a);
    }
  });
  return result;
}


}

