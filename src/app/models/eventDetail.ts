export class EventDetail {  

    id:{ type: String; } | undefined;
    title: { type: String; } | undefined;
    lead_text: { type: String; } | undefined;
    description: { type: String; } | undefined;

    category:{ type: String; } | undefined;
    date_start: { type: Date; } | undefined;
    date_end: { type: Date; } | undefined;

    address_name:{ type: String; } | undefined;
    address_street: { type: String; } | undefined;
    address_zipcode:{ type: String; } | undefined;
    transport:{ type: String; } | undefined;

    access_type: { type: String; } | undefined;
    price_type:{ type: String; } | undefined;

    contact_name:{ type: String; } | undefined;
    contact_phone:{ type: String; } | undefined;
    contact_mail:{ type: String; } | undefined;
    contact_url:{ type: String; } | undefined;

    cover:{ type: Object; } | undefined;
    cover_url:{ type: String; } | undefined;

    geometry:{ type: Object; } | undefined
}