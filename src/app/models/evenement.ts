export interface IEvenement {

    id:{type:String},
    title: {type:String},
    lead_text: {type:String},
    description: {type:String},
   
    category:{type:String},
    date_start: {type: Date},
    date_end: {type: Date},

    address_name:{type:String},
    address_street: {type:String},
    address_zipcode:{type:String},
    transport:{type:String},

    access_type: {type: String},
    price_type:{type: String},
    price_detail :{type: String},

    contact_name:{type:String},
    contact_phone:{type:String},
    contact_mail:{type:String},
    contact_url:{type:String},
    
    cover:{type:Object},
    cover_url:{type:String},

    geometry:{type:Object}


}